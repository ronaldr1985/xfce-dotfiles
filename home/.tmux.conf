#   _____                   _     _ __  ___   ___  _____
#  |  __ \                 | |   | /_ |/ _ \ / _ \| ____|
#  | |__) |___  _ __   __ _| | __| || | (_) | (_) | |__
#  |  _  // _ \| '_ \ / _` | |/ _` || |\__, |> _ <|___ \
#  | | \ \ (_) | | | | (_| | | (_| || |  / /| (_) |___) |
#  |_|  \_\___/|_| |_|\__,_|_|\__,_||_| /_/  \___/|____/
#
# Filename: ~/.tmux.conf
# GitLab: https://gitlab.com/ronaldr1985/dot-files
# Maintainer: Ronald

# Stuff for VIM {{{

set -g default-terminal "tmux-256color"
set-window-option -g xterm-keys on

# }}}

# Keybindings {{{

# Set the prefix to CTRL-e
set-option -g prefix C-e
bind-key C-e send-prefix

# Set prefix+r to reload tmux config
bind r source-file ~/.tmux.conf \; display-message "reloaded tmux config file"

# Move panes with prefix+[hjkl]
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# Moving between panes with CTRL+[hjkl] but still allowing this to work in vim
is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
bind-key -n 'C-h' if-shell "$is_vim" 'send-keys C-h'  'select-pane -L'
bind-key -n 'C-j' if-shell "$is_vim" 'send-keys C-j'  'select-pane -D'
bind-key -n 'C-k' if-shell "$is_vim" 'send-keys C-k'  'select-pane -U'
bind-key -n 'C-l' if-shell "$is_vim" 'send-keys C-l'  'select-pane -R'
tmux_version='$(tmux -V | sed -En "s/^tmux ([0-9]+(.[0-9]+)?).*/\1/p")'
if-shell -b '[ "$(echo "$tmux_version < 3.0" | bc)" = 1 ]' \
    "bind-key -n 'C-\\' if-shell \"$is_vim\" 'send-keys C-\\'  'select-pane -l'"
if-shell -b '[ "$(echo "$tmux_version >= 3.0" | bc)" = 1 ]' \
    "bind-key -n 'C-\\' if-shell \"$is_vim\" 'send-keys C-\\\\'  'select-pane -l'"

bind-key -T copy-mode-vi 'C-h' select-pane -L
bind-key -T copy-mode-vi 'C-j' select-pane -D
bind-key -T copy-mode-vi 'C-k' select-pane -U
bind-key -T copy-mode-vi 'C-l' select-pane -R
bind-key -T copy-mode-vi 'C-\' select-pane -l

# switch panes using ALT+[hjkl]
bind -n M-l resize-pane -R 5
bind -n M-h resize-pane -L 5
bind -n M-k resize-pane -U 5
bind -n M-j resize-pane -D 5

# Change working directory with CTRL-p
bind -n C-p attach -c "#{pane_current_path}" \; display-message "changed working directory"

# paste
bind P paste-buffer

# copy to external clipboard
bind -Tcopy-mode-vi c send -X copy-pipe "xclip -i"

# }}}

# copy/vi Mode {{{

# Enable vi keys for copy mode
setw -g mode-keys vi
bind-key -T copy-mode-vi 'v' send -X begin-selection
bind-key -Tcopy-mode-vi 'C-v' send -X begin-selection \; send -X rectangle-toggle
bind-key -T copy-mode-vi 'y' send -X copy-selection

# }}}

# Mouse {{{

# Enable mouse mode (tmux 2.1 and above)
set -g mouse on

# }}}

# The look and feel stuff {{{

# General stuff
set-option -g status on
set-option -g status-interval 1
set-option -g status-style fg=colour136
set-option -g status-position bottom

# Change how the window status looks
set -g window-status-current-format '#[fg=blue](#I #W)'
set -g window-status-format '#[fg=white](#I #W)'

# Make the background of the status bar transparent
set-option -g status-style bg=default

# Set the length of the left part of the status bar
set-option -g status-left-length 60

# Setting what shows up on what the left of the status line
set-option -g status-left "#[fg=green]#(whoami)  |  "

# Set the length of the right part of the status bar
set-option -g status-right-length 60

# Setting what shows up on what the right of the status line
set-option -g status-right '#[fg=green]%A %d %B %Y  |  %H:%M:%S  |  #(hostname | sed "s/\..*//")'

# }}}

# Behaviour {{{

set-option -s escape-time 0

set -s set-clipboard off

set -s copy-command 'xsel -i'

# }}}

# vim:foldmethod=marker:foldlevel=0



