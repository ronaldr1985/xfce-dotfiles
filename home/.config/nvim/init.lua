--  _       _ _     _
-- (_)_ __ (_) |_  | |_   _  __ _
-- | | '_ \| | __| | | | | |/ _` |
-- | | | | | | |_ _| | |_| | (_| |
-- |_|_| |_|_|\__(_)_|\__,_|\__,_|
--
-- Filename: .config/nvim/init.vim
-- Git Repo: https://gitlab.com/ronaldr1985/dot-files
-- Maintainer: Ronald1985

-- Variables {{{

local vim = vim
local api = vim.api

-- }}}

-- Plugins {{{

local Plug = vim.fn['plug#']

-- Specify a directory for plugins
vim.call('plug#begin')
Plug('faith/vim-go', {on = 'GoUpdateBinaries'})
Plug('preservim/nerdcommenter')
Plug('itchyny/vim-cursorword')
Plug('jiangmiao/auto-pairs')
Plug('junegunn/fzf')
Plug('junegunn/fzf.vim')
Plug('sheerun/vim-polyglot')
Plug('christoomey/vim-tmux-navigator')
Plug('ntpeters/vim-better-whitespace')
Plug('pearofducks/ansible-vim')
Plug('dense-analysis/ale')
Plug('nvim-lualine/lualine.nvim')
Plug('nvim-tree/nvim-web-devicons')
Plug('morhetz/gruvbox')
Plug('mcchrish/nnn.vim')
Plug('skywind3000/asynctasks.vim')
Plug('skywind3000/asyncrun.vim')
Plug('shmup/vim-sql-syntax')
Plug('Shirk/vim-gas')
Plug('Tetralux/odin.vim')
Plug('sudar/vim-arduino-syntax')
Plug('junegunn/goyo.vim')
Plug('neovim/nvim-lspconfig')
Plug('altercation/vim-colors-solarized')
vim.call('plug#end')

-- }}}

-- LuaLine Settings {{{

require('lualine').setup {
	options = {
		icons_enabled = true,
		theme = 'gruvbox_dark',
		component_separators = { left = '', right = ''},
		section_separators = { left = '', right = ''},
		disabled_filetypes = {
			statusline = {},
			winbar = {},
		},
		ignore_focus = {},
		always_divide_middle = true,
		globalstatus = false,
		refresh = {
			statusline = 1000,
			tabline = 1000,
			winbar = 1000,
		}
	},
	sections = {
		lualine_a = {'mode'},
		lualine_b = {'branch', 'diff', 'diagnostics'},
		lualine_c = {'filename'},
		lualine_x = {'encoding', 'fileformat', 'filetype'},
		lualine_y = {'progress'},
		lualine_z = {'location'}
	},
	inactive_sections = {
		lualine_a = {},
		lualine_b = {},
		lualine_c = {'filename'},
		lualine_x = {'location'},
		lualine_y = {},
		lualine_z = {}
	},
	tabline = {},
	winbar = {},
	inactive_winbar = {},
	extensions = {}
}

-- }}}

-- NerdCommenter Settings {{{

-- Add spaces after comment delimiters by default
api.nvim_set_var("NERDSpaceDelims", 1)

-- Use compact syntax for prettified multi-line comments
api.nvim_set_var("NERDCompactSexyComs ", 1)

-- Disable the default NERDCommenter keybinds
api.nvim_set_var("NERDCreateDefaultMappings ", 0)

-- }}}

-- Random {{{

api.nvim_command [[set encoding=utf-8]]

-- maintain undo history between sessions
vim.cmd([[
set undofile
]])

-- }}}

-- Colours {{{

api.nvim_command [[syntax on]]
api.nvim_command [[set background=dark]]
api.nvim_command [[set termguicolors]]
api.nvim_command [[colorscheme gruvbox]]

-- }}}

-- UI {{{

-- Set relative number
vim.wo.relativenumber = true

-- Still show the actual line number though
vim.wo.number = true

-- Colour column
vim.wo.colorcolumn = "120"

-- i sometimes use the mouse to scroll through a buffer
vim.cmd [[set mouse=a]]

api.nvim_command [[:set list lcs=tab:\.\ ]]

-- Splits open at the bottom and the right
api.nvim_command [[set splitbelow splitright]]

-- }}}

-- Keybindings {{{

vim.g.mapleader = ","

-- NERDCommenter
api.nvim_set_keymap("n", "<C-/>", ":call nerdcommenter#Comment(0, 'toggle')<CR>", {noremap = true})
api.nvim_set_keymap("v", "<C-/>", ":call nerdcommenter#Comment(0, 'toggle')<CR>", {noremap = true})
api.nvim_set_keymap("n", "", ":call nerdcommenter#Comment(0, 'toggle')<CR>", {noremap = true})
api.nvim_set_keymap("v", "", ":call nerdcommenter#Comment(0, 'toggle')<CR>", {noremap = true})
api.nvim_set_keymap("n", "<leader>/", ":call nerdcommenter#Comment(0, 'toggle')<CR>", {noremap = true})
api.nvim_set_keymap("v", "<leader>/", ":call nerdcommenter#Comment(0, 'toggle')<CR>", {noremap = true})

-- Map spell check to <leader>o
api.nvim_set_keymap("n", "<leader>o", ":setlocal spell! spelllang=en_gb<CR>", {noremap=true})

-- Map <leader>v to vsplit
api.nvim_set_keymap("n", "<leader>v", ":vsplit<CR>", {noremap=true})

-- Control [hjkl] moves between splits
api.nvim_set_keymap("n", "<C-h>", "<C-W><C-h>", {noremap=true})
api.nvim_set_keymap("n", "<C-j>", "<C-W><C-j>", {noremap=true})
api.nvim_set_keymap("n", "<C-k>", "<C-W><C-k>", {noremap=true})
api.nvim_set_keymap("n", "<C-l>", "<C-W><C-l>", {noremap=true})

-- Clear vim search
api.nvim_set_keymap("n", "<leader>z", ":noh<CR>", {noremap=true})

-- Toggle vim ALE
api.nvim_set_keymap("n", "<leader>a", ":ALEToggle<CR>", {noremap=true})

-- Goyo plugin makes text more readable when writing prose:
api.nvim_set_keymap("n", "<leader>a", ":Goyo<CR>", {noremap=true})

-- Toggle autopairs
api.nvim_command [[let g:AutoPairsShortcutToggle = '<F3>']]

-- Show fzf search for files in current directory
api.nvim_set_keymap("n", "<F4>", ":Files<CR>", {noremap=true, silent=true})
api.nvim_set_keymap("n", "<C-f>", ":Files<CR>", {noremap=true, silent=true})

-- Search in all files in directory
api.nvim_set_keymap("n", "<leader>f", ":Rg<CR>", {noremap=true, silent=true})

-- Search vim buffers
api.nvim_set_keymap("n", "<leader>b", ":Rg<CR>", {noremap=true, silent=true})

-- Close current buffer
api.nvim_set_keymap("n", "<leader>q", ":bp<BAR>bd#<CR>", {noremap=true, silent=true})

-- Create vertical split from current buffer
api.nvim_set_keymap("n", "<leader>t", ":bp<BAR>bd#<CR>", {noremap=true, silent=true})

-- Go to tab by number
api.nvim_set_keymap("n", "<leader>1", "1gt", {noremap=true})
api.nvim_set_keymap("n", "<leader>2", "2gt", {noremap=true})
api.nvim_set_keymap("n", "<leader>3", "3gt", {noremap=true})
api.nvim_set_keymap("n", "<leader>4", "4gt", {noremap=true})
api.nvim_set_keymap("n", "<leader>5", "5gt", {noremap=true})
api.nvim_set_keymap("n", "<leader>6", "6gt", {noremap=true})
api.nvim_set_keymap("n", "<leader>7", "7gt", {noremap=true})
api.nvim_set_keymap("n", "<leader>8", "8gt", {noremap=true})
api.nvim_set_keymap("n", "<leader>9", "9gt", {noremap=true})

-- Replace tabs with spaces
api.nvim_set_keymap("n", "<leader>`", ":set expandtab<CR>", {noremap=true})

-- Edit AsyncTasks config file for current project
api.nvim_set_keymap("n", "<leader>w", ":AsyncTaskEdit<CR>", {noremap=true})

-- Compile current project
api.nvim_set_keymap("n", "<leader>c", ":AsyncTask file-build<CR>", {noremap=true})

-- Run current project
api.nvim_set_keymap("n", "<leader>r", ":AsyncTask file-run<CR>", {noremap=true})

-- }}}

-- File stuff {{{

vim.cmd [[autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man set filetype=groff]]
vim.cmd [[autocmd BufRead,BufNewFile *.tex set filetype=tex]]
vim.cmd [[autocmd BufRead,BufNewFile *.h,*.c set filetype=c]]
vim.cmd [[autocmd BufRead *.sql set filetype=mysql]]
vim.cmd [[au BufRead,BufNewFile *.yml,*.yaml set filetype=yaml.ansible]]
vim.cmd [[autocmd BufEnter *.yml,*.yaml ALEDisable]] -- Turn off ALE for yaml files
vim.cmd [[autocmd BufRead,BufNewFile Jenkinsfile set filetype=groovy]]
vim.cmd [[autocmd BufRead *.odin set filetype=odin]]

vim.o.tabstop = 4
vim.o.softtabstop = 4
vim.o.shiftwidth = 4
vim.o.smarttab = true

-- Case insensitive searches
vim.o.ignorecase = true

-- but still want search to be smart. If i type a upper case thing, do a case
-- sensitive blah
vim.o.smartcase = true

-- }}}

-- Folding {{{

vim.cmd [[set foldenable]]
vim.cmd[[set foldlevelstart=10]]
vim.cmd[[set foldnestmax=10]]
vim.cmd[[set foldmethod=syntax]]
 -- Open and close all folds
vim.cmd[[nnoremap <space> za]]
vim.cmd[[nnoremap <space> za]]

-- }}}

-- fzf {{{

vim.cmd(
	[[
command! -bang -nargs=* Rg call fzf#vim#grep("rg --column --line-number --no-heading --color=always --smart-case ".shellescape(<q-args>), 1, {'options': '--delimiter : --nth 4..'}, <bang>0)
	]]
)

-- }}}

-- Language servers {{{

local lspconfig = require('lspconfig')
local lsp = require('lspconfig')

-- Go Language Server
lspconfig.gopls.setup({
	settings = {
		gopls = {
			analyses = {
				unusedparams = true,
			},
			staticcheck = true,
			gofumpt = true,
		},
	},
})

-- Odin Language server
lspconfig.ols.setup({})

-- }}}

-- init.vim - Sourcing and editing {{{

api.nvim_set_keymap("n", "<leader>ev", ":e ~/.config/nvim/init.lua<CR>", {noremap=true})
api.nvim_set_keymap("n", "<leader>sv", ":source ~/.config/nvim/init.lua <bar> :doautocmd BufRead<CR>", {noremap=true})

-- }}}

-- Ansible {{{

-- Indentation will completely reset (unindent to column 0)
vim.cmd[[let g:ansible_unindent_after_newline = 1]]

-- Dim the instances of name: found
vim.cmd[[let g:ansible_name_highlight = 'b']]

-- }}}

-- asynctasks.vim {{{

-- Open a quickfix window automatically when we build/run
vim.cmd [[let g:asyncrun_open = 6]]

-- }}}

-- Terminal stuff {{{

vim.cmd([[
" Terminal Function
let g:term_buf = 0
let g:term_win = 0
function! TermToggle(height)
    if win_gotoid(g:term_win)
        hide
    else
        botright new
        exec "resize " . a:height
        try
            exec "buffer " . g:term_buf
        catch
            call termopen($SHELL, {"detach": 0})
            let g:term_buf = bufnr("")
            set nonumber
            set norelativenumber
            set signcolumn=no
        endtry
        startinsert!
        let g:term_win = win_getid()
    endif
endfunction
]])

vim.cmd([[
" Toggle terminal on/off (neovim)
nnoremap <C-'> :call TermToggle(16)<CR>
inoremap <C-'> <Esc>:call TermToggle(16)<CR>
tnoremap <C-'> <C-\><C-n>:call TermToggle(16)<CR>
nnoremap <A-'> :call TermToggle(16)<CR>
inoremap <A-'> <Esc>:call TermToggle(16)<CR>
tnoremap <A-'> <C-\><C-n>:call TermToggle(16)<CR>
nnoremap <A-`> :call TermToggle(16)<CR>
inoremap <A-`> <Esc>:call TermToggle(16)<CR>
tnoremap <A-`> <C-\><C-n>:call TermToggle(16)<CR>

" Terminal go back to normal mode
tnoremap <Esc> <C-\><C-n>
tnoremap :q! <C-\><C-n>:q!<CR>
]])

-- }}}

-- vim:foldmethod=marker:foldlevel=0

